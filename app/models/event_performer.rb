# == Schema Information
#
# Table name: event_performers
#
#  id           :integer          not null, primary key
#  event_id     :integer
#  performer_id :integer
#  created_at   :datetime
#  updated_at   :datetime
#

class EventPerformer < ActiveRecord::Base
  belongs_to :event
  belongs_to :performer
end
