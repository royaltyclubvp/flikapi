# == Schema Information
#
# Table name: event_ratings
#
#  id         :integer          not null, primary key
#  event_id   :integer
#  user_id    :integer
#  artistes   :decimal(10, 2)
#  djs        :decimal(10, 2)
#  food       :decimal(10, 2)
#  drinks     :decimal(10, 2)
#  venue      :decimal(10, 2)
#  created_at :datetime
#  updated_at :datetime
#  total      :decimal(10, 2)
#

class EventRating < ActiveRecord::Base
  # Belongs to an Event
  belongs_to :event, counter_cache: true

  # Submitted by a User
  belongs_to :user, inverse_of: :event_ratings

  after_commit :update_event_rating_cache

  before_save :calculate_total

  protected

    def remove_previous_rating
      unless self.new_record?
        self.event.rating = self.event.rating - self.total
        self.event.save
      end
    end

    def update_event_rating_cache
      self.event.rating = self.event.rating + self.total
      self.event.save
    end

    def calculate_total
      unless self.new_record?
        self.remove_previous_rating
      end
      total_c = (self.artistes + self.drinks + self.venue + self.djs)
      if !self.food.nil?
        total_c += self.food
        self.total = total_c / 5
      else
        self.total = total_c / 4
      end
    end
end
