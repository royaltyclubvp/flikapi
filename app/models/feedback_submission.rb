# == Schema Information
#
# Table name: feedback_submissions
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  content    :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class FeedbackSubmission < ActiveRecord::Base
  # Feedback belongs to a User
  belongs_to :user, inverse_of: :feedback_submissions
end
