# == Schema Information
#
# Table name: event_list_entries
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  first_name :string
#  last_name  :string
#  checked    :boolean          default(FALSE)
#  event_id   :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class EventListEntry < ActiveRecord::Base
  # Belongs to an Event
  belongs_to :event, inverse_of: :event_list_entries

  # Belongs to a User
  belongs_to :user, inverse_of: :event_list_entries
end
