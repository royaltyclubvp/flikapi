# == Schema Information
#
# Table name: api_keys
#
#  id           :integer          not null, primary key
#  access_token :string           not null, indexed
#  expires_at   :datetime
#  user_id      :integer          not null, indexed
#  active       :boolean          default(TRUE), not null
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#
# Indexes
#
#  index_api_keys_on_access_token  (access_token) UNIQUE
#  index_api_on_user_id            (user_id)
#


class ApiKey < ActiveRecord::Base
  before_create :generate_access_token
  before_create :set_expiration

  # Belongs to User
  belongs_to :user, inverse_of: :api_keys

  def expired?
    DateTime.now >= self.expires_at
  end

  private
    def generate_access_token
      begin
        self.access_token = SecureRandom.hex
      end while self.class.exists?(access_token: access_token)
    end

    def set_expiration
      self.expires_at = DateTime.now + 30.day
    end
end
