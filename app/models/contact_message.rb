# == Schema Information
#
# Table name: contact_messages
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  subject    :string
#  message    :text
#  read       :boolean          default(FALSE)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class ContactMessage < ActiveRecord::Base
  belongs_to :user, inverse_of: :contact_messages
end
