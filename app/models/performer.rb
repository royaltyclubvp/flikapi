# == Schema Information
#
# Table name: performers
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime
#  updated_at :datetime
#

class Performer < ActiveRecord::Base
  has_many :event_performers
  has_many :events, :through => :event_performers
end
