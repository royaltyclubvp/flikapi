# == Schema Information
#
# Table name: promoters
#
#  id                     :integer          not null, primary key
#  email                  :string           default(""), not null, indexed
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string           indexed
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :inet
#  last_sign_in_ip        :inet
#  confirmation_token     :string           indexed
#  confirmed_at           :datetime
#  confirmation_sent_at   :datetime
#  unconfirmed_email      :string
#  failed_attempts        :integer          default(0), not null
#  unlock_token           :string           indexed
#  locked_at              :datetime
#  created_at             :datetime
#  updated_at             :datetime
#  name                   :string
#
# Indexes
#
#  index_promoters_on_confirmation_token    (confirmation_token) UNIQUE
#  index_promoters_on_email                 (email) UNIQUE
#  index_promoters_on_reset_password_token  (reset_password_token) UNIQUE
#  index_promoters_on_unlock_token          (unlock_token) UNIQUE
#

class Promoter < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :lockable

  # Has many events
  has_many :events, inverse_of: :promoter

  # Has many submitted events
  has_many :submitted_events, -> { where(status: 0).order(submission_date: :asc) }, class_name: 'Event'

  # Has many approved events
  has_many :approved_events, -> { where(status: 1).order(start_time: :asc) }, class_name: 'Event'

  # Has many declined events
  has_many :declined_events, -> { where(status: 2).order(submission_date: :asc) }, class_name: 'Event'

  # Has Many Past Events
  has_many :past_events, -> { approved.where("start_time < ?", (DateTime.now - 1.day)).order('start_time DESC') }, class_name: 'Event'

  # Has Many Upcoming Events
  has_many :upcoming_events, -> { approved.where("start_time > ?", (DateTime.now + 30.minutes)).order('start_time ASC') }, class_name: 'Event'

  # Has Many Current Events
  has_many :current_events, -> { approved.where("start_time < ? AND start_time > ?", (DateTime.now + 30.minutes), (DateTime.now - 1.day)).order('start_time ASC') }, class_name: 'Event'

end
