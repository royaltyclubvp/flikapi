# == Schema Information
#
# Table name: user_fliks
#
#  id         :integer          not null, primary key
#  event_id   :integer
#  user_id    :integer
#  created_at :datetime
#  updated_at :datetime
#

class UserFlik < ActiveRecord::Base
  belongs_to :event
end
