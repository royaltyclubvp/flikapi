# == Schema Information
#
# Table name: events
#
#  id                  :integer          not null, primary key
#  name                :string
#  start_time          :datetime
#  location_id         :string
#  ticket_price        :integer
#  facebook_page       :string
#  description         :text
#  twitter_page        :string
#  ig_page             :string
#  created_at          :datetime
#  updated_at          :datetime
#  event_type_id       :integer
#  event_flyer         :string
#  rating              :decimal(10, 2)
#  event_ratings_count :integer          default(0), not null
#  promoter_id         :integer
#  list_enabled        :boolean          default(FALSE)
#  featured            :boolean          default(FALSE)
#  status              :integer
#  declined_reason     :text
#  submission_date     :datetime
#  country             :integer          indexed
#

require 'action_view'

class Event < ActiveRecord::Base

  require 'action_view'

  include ActionView::Helpers::DateHelper

  #Apply Carrierwave Uploader for Image
  mount_uploader :event_flyer, FlyerUploader

  # Set Values for Status Column using Enum feature
  enum status: [:submitted, :approved, :declined, :removed]

  # Set Values for Country Column using Enum feature
  # 0 => Trinidad & Tobago
  # 1 => Jamaica
  # 2 => Barbados
  enum country: [:tt, :ja, :bim]

  # Set Values for Event Category using Enum feature
  # 0 => Arts
  # 1 => Sports & Fitness
  # 2 => Fashion
  # 3 => Business
  # 4 => Travel & Outdoor
  # 5 => Nightlife
  # 6 => Concert
  # 7 => Other
  enum event_category: [:arts, :sportsfitness, :fashion, :business, :traveloutdoor, :nightlife, :concert, :other]

  # Set Default for Status
  def set_default_status
    self.status ||= :approved
  end

  def set_default_rating
    self.rating ||= 0
  end

  # Set Status upon initialization
  after_initialize :set_default_status, if: :new_record?
  # Set Rating upon initialization
  after_initialize :set_default_rating, if: :new_record?

  # Validate Ticket Price before Save
  before_save :is_price_unspecified

  scope :submitted, -> {where(status: 0).order(submission_date: :asc)}

  scope :approved, -> {where(status: 1)}

  scope :declined, -> {where(status: 2).order(submission_date: :asc)}

  scope :published, -> { approved.where(published: true) }

  # Has events that have passed
  scope :passed, -> { published.where("start_time < ?", (DateTime.now - 1.day)).order('start_time DESC') }

  # Has events that are current
  scope :current, -> { published.where("start_time < ? AND start_time > ?", (DateTime.now + 30.minutes), (DateTime.now - 1.day)).order('start_time ASC') }

  # Has events that are upcoming
  scope :upcoming, -> { published.where("start_time > ?", (DateTime.now + 30.minutes)).order('start_time ASC') }

  # Events that are featured
  scope :featured, -> { published.where(featured: true)}

  # Upcoming events that are featured
  scope :upcoming_featured, -> { published.where(featured: true) }

  has_many :event_performers

  has_many :performers, :through => :event_performers

  accepts_nested_attributes_for :event_performers,
                                :allow_destroy => true
  belongs_to :location

  belongs_to :event_type

  belongs_to :promoter, inverse_of: :events

  has_many :event_list_entries, -> { order(last_name: :asc)}, inverse_of: :event

  has_many :event_ratings

  has_many :user_fliks

  def time_to_event
    distance_of_time_in_words(Time.now, start_time)
  end

  def is_price_unspecified
    if self.ticket_price == ' '
      self.ticket_price = nil
    end
  end

  def as_json(options={})
    super(:methods => [:time_to_event], include: [:performers, :location])
  end
end
