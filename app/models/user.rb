# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string           default(""), not null, indexed
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string           indexed
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string
#  last_sign_in_ip        :string
#  created_at             :datetime
#  updated_at             :datetime
#  first_name             :string
#  last_name              :string
#
# Indexes
#
#  index_users_on_email                 (email) UNIQUE
#  index_users_on_reset_password_token  (reset_password_token) UNIQUE
#

class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :rememberable, :trackable, :validatable, :recoverable

  # Set Values for Country as Enum
  # 0 => Trinidad & Tobago
  # 1 => Jamaica
  # 2 => Barbados
  enum country: [:tt, :ja, :bim]

  # Set Default for Status
  def set_default_country
    self.country ||= 'tt'
  end

  # Set Status upon initialization
  after_initialize :set_default_country, if: :new_record?

  # Has many API Keys
  has_many :api_keys, inverse_of: :user

  has_many :user_fliks

  has_many :event_list_entries, inverse_of: :user

  has_many :contact_messages, inverse_of: :user

  has_many :feedback_submissions, inverse_of: :user

  has_many :event_ratings, inverse_of: :user

  def as_json(options={})
    super(:except => [:encrypted_password])
  end

  def name
    first_name + ' ' + last_name
  end
end

