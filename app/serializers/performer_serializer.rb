class PerformerSerializer < ActiveModel::Serializer
  attributes :id, :name
end
