class EventRatingSerializer < ActiveModel::Serializer
  attributes :id, :artistes, :djs, :drinks, :food, :venue, :total, :event_id
end
