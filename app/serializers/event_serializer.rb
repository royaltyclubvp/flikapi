class EventSerializer < ActiveModel::Serializer
  attributes :id, :name, :event_category, :description, :event_flyer_url, :event_ratings_count, :facebook_page, :ig_page, :twitter_page, :list_enabled, :rating, :start_time, :ticket_price, :event_location,
             :event_type, :event_type_id, :featured, :country

  has_many :performers

  def event_location
    if object.location.nil?
      'TBA'
    else
      object.location.name
    end
  end

  def event_type
    if object.event_type.nil?
      'Unknown'
    else
      object.event_type.name
    end
  end

  def event_flyer_url
    if object.event_flyer.url == 'https://d3b4tdf4w0xsi2.cloudfront.net/2015/default-event-flyer.jpg'
      'https://d3b4tdf4w0xsi2.cloudfront.net/default-event-flyer.jpg'
    else
      object.event_flyer.url
    end
  end
end
