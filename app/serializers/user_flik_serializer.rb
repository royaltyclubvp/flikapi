class UserFlikSerializer < ActiveModel::Serializer
  attributes :id, :event_id

  has_one :event
end
