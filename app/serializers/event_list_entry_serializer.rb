class EventListEntrySerializer < ActiveModel::Serializer
  attributes :id, :event_id
end
