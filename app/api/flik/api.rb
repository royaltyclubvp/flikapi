module Flik
  class API < Grape::API
    use Rack::JSONP
    version 'v1', using: :accept_version_header
    format :json
    formatter :json, Grape::Formatter::ActiveModelSerializers

    helpers do
      def authenticate!
        error!('Unauthorized', 401) unless current_user
      end

      def current_user
        token = ApiKey.where(access_token: params[:token]).first
        if !token.nil? && !token.expired?
          @current_user = token.user
        else
          false
        end
      end
    end

    rescue_from ActiveRecord::RecordNotFound do |e|
      error_response(message: e.message, status: 404)
    end

    rescue_from :all do |e|
      if Rails.env.development?
        raise e
      else
        Raven.capture_exception(e)
        error_response(message: 'Internal server error', status: 500)
      end
    end

    # Event Endpoints
    resource :events, desc: 'Event Endpoints' do
      desc 'Return the list of upcoming events [Default Country is Trinidad]'
      get '/upcoming/' do
=begin
          if (events = $redis.get('upcoming/tt')).nil?
              events = Event.includes(:location, :event_type, :performers).upcoming.tt
              # Set Time to Change to Time of Next Event
              time_to_change = events.first.start_time
              $redis.setex('upcoming/tt', (time_to_change - Time.now()).to_i, events.to_json)
          else
              events = JSON.parse(events)
          end
          events
=end
          Event.includes(:location, :event_type, :performers).upcoming.tt
      end

      desc 'Return the complete list of upcoming events [All Countries]'
      get '/upcoming/all' do
=begin
          if (events = $redis.get('upcoming/all')).nil?
              events = Event.includes(:location, :event_type, :performers).upcoming
              # Set Time to Change to Time of Next Event
              time_to_change = events.first.start_time
              $redis.setex('upcoming/all', (time_to_change - Time.now()).to_i, events.to_json)
          else
              events = JSON.parse(events)
          end
          events
=end
          Event.includes(:location, :event_type, :performers).upcoming
      end

      desc 'Return the list of upcoming events with user token [Events will be retrieved according to User Country Setting]'
      params do
        requires :token, type: String, desc: 'User Token'
      end
      get '/upcoming/user/:token' do
        authenticate!
=begin
        if (events = $redis.get('upcoming/' + @current_user.id)).nil?
            if @current_user.country == 'tt'
              events = Event.includes(:location, :event_type, :performers).upcoming.tt
            elsif @current_user.country == 'ja'
              events = Event.includes(:location, :event_type, :performers).upcoming.ja
            elsif @current_user.country == 'bim'
              events = Event.includes(:location, :event_type, :performers).upcoming.bim
            end
            # Set Time to Change to Time of Next Event
            time_to_change = events.first.start_time
            $redis.setex('upcoming/' + @current_user.id, (time_to_change - Time.now()).to_i, events.to_json)
        else
            events = JSON.parse(events)
        end
=end
        if @current_user.country == 'tt'
          events = Event.includes(:location, :event_type, :performers).upcoming.tt
        elsif @current_user.country == 'ja'
          events = Event.includes(:location, :event_type, :performers).upcoming.ja
        elsif @current_user.country == 'bim'
          events = Event.includes(:location, :event_type, :performers).upcoming.bim
        end
        events
      end

      desc 'Return the list of upcoming events for a particular country'
      params do
        requires :country, type: String, desc: 'Country Code'
      end
      get '/upcoming/:country' do
        if params[:country] == 'tt'
          Event.includes(:location, :event_type, :performers).upcoming.tt
        elsif params[:country] == 'ja'
          Event.includes(:location, :event_type, :performers).upcoming.ja
        elsif params[:country] == 'bim'
          Event.includes(:location, :event_type, :performers).upcoming.bim
        end
      end

      desc 'Return the list of upcoming featured events [Default Country is Trinidad]'
      get '/featured/' do
=begin
          if (events = $redis.get('featured_events/tt')).nil?
              events = Event.includes(:location, :event_type, :performers).upcoming_featured.tt
              time_to_change = events.first.start_time
              $redis.setex('featured_events/tt', (time_to_change - Time.now()).to_i, events.to_json)
          else
              events = JSON.parse(events)
          end
=end
          Event.includes(:location, :event_type, :performers).upcoming_featured.tt
      end

      desc 'Return the complete list of featured events'
      get '/featured/all' do
        Event.includes(:location, :event_type, :performers).upcoming_featured
      end

      desc 'Return the list of upcoming featured events with user token [Events will be retrieved according to User Country Setting]'
      params do
        requires :token, type: String, desc: 'User Token'
      end
      get '/featured/user/:token' do
        authenticate!
        if @current_user.country == 'tt'
          Event.includes(:location, :event_type, :performers).upcoming_featured.tt
        elsif @current_user.country == 'ja'
          Event.includes(:location, :event_type, :performers).upcoming_featured.ja
        elsif @current_user.country == 'bim'
          Event.includes(:location, :event_type, :performers).upcoming_featured.bim
        end
      end

      desc 'Return the list of upcoming featured events for a particular country'
      params do
        requires :country, type: String, desc: 'Country Code'
      end
      get '/featured/:country' do
        if params[:country] == 'tt'
          Event.includes(:location, :event_type, :performers).upcoming_featured.tt
        elsif params[:country] == 'ja'
          Event.includes(:location, :event_type, :performers).upcoming_featured.ja
        elsif params[:country] == 'bim'
          Event.includes(:location, :event_type, :performers).upcoming_featured.bim
        end
      end

      desc 'Return the list of upcoming events of a particular type [Default Country is Trinidad]'
      params do
        requires :event_type_id, type: Integer, desc: 'Event Type ID'
      end
      get '/type/:event_type_id' do
        Event.includes(:location, :event_type, :performers).where({event_type_id: params[:event_type_id]}).upcoming.tt
      end

      desc 'Return the list of upcoming events of a particular type for a particular country with user token [Events will be retrieved according to User Country Setting]'
      params do
        requires :event_type_id, type: Integer, desc: 'Event Type ID'
        requires :token, type: String, desc: 'User Token'
      end
      get '/type/:event_type_id/user/:token' do
        authenticate!
        if @current_user.country == 'tt'
          Event.includes(:location, :event_type, :performers).where({event_type_id: params[:event_type_id]}).upcoming.tt
        elsif @current_user.country == 'ja'
          Event.includes(:location, :event_type, :performers).where({event_type_id: params[:event_type_id]}).upcoming.ja
        elsif @current_user.country == 'bim'
          Event.includes(:location, :event_type, :performers).where({event_type_id: params[:event_type_id]}).upcoming.bim
        end
      end

      desc 'Return the list of upcoming events of a particular type for a particular country'
      params do
        requires :event_type_id, type: Integer, desc: 'Event Type ID'
        requires :country, type: String, desc: 'Country Code'
      end
      get '/type/:event_type_id/:country' do
        if params[:country] == 'tt'
          Event.includes(:location, :event_type, :performers).where({event_type_id: params[:event_type_id]}).upcoming.tt
        elsif params[:country] == 'ja'
          Event.includes(:location, :event_type, :performers).where({event_type_id: params[:event_type_id]}).upcoming.ja
        elsif params[:country] == 'bim'
          Event.includes(:location, :event_type, :performers).where({event_type_id: params[:event_type_id]}).upcoming.bim
        end
      end


      desc 'Return the list of events between specified dates [Default country is Trinidad]'
      params do
        requires :start_date, type: String, desc: 'Start Date in Range', documentation: {example: '10-14-2015'}
        requires :end_date, type: String, desc: 'End Date in Range', documentation: {example: '10-14-2015'}
      end
      get 'from/:start_date/to/:end_date' do
        Event.includes(:performers, :location).order(start_time: :asc).where(start_time: params[:start_date]..params[:end_date]).tt
      end

      desc 'Return the list of events between specified dates for a particular country'
      params do
        requires :start_date, type: String, desc: 'Start Date in Range', documentation: {example: '10-14-2015'}
        requires :end_date, type: String, desc: 'End Date in Range', documentation: {example: '10-14-2015'}
        requires :token, type: String, desc: 'User Token'
      end
      get 'from/:start_date/to/:end_date/user/:token' do
        authenticate!
        if @current_user.country == 'tt'
          Event.includes(:performers, :location).order(start_time: :asc).where(start_time: params[:start_date]..params[:end_date]).tt
        elsif @current_user.country == 'ja'
          Event.includes(:performers, :location).order(start_time: :asc).where(start_time: params[:start_date]..params[:end_date]).ja
        elsif @current_user.country == 'bim'
          Event.includes(:performers, :location).order(start_time: :asc).where(start_time: params[:start_date]..params[:end_date]).bim
        end
      end

      desc 'Return the list of events between specified dates for a particular country'
      params do
        requires :start_date, type: String, desc: 'Start Date in Range', documentation: {example: '10-14-2015'}
        requires :end_date, type: String, desc: 'End Date in Range', documentation: {example: '10-14-2015'}
        requires :country, type: String, desc: 'Country Code'
      end
      get 'from/:start_date/to/:end_date/:country' do
        if params[:country] == 'tt'
          Event.includes(:performers, :location).order(start_time: :asc).where(start_time: params[:start_date]..params[:end_date]).tt
        elsif params[:country] == 'ja'
          Event.includes(:performers, :location).order(start_time: :asc).where(start_time: params[:start_date]..params[:end_date]).ja
        elsif params[:country] == 'bim'
          Event.includes(:performers, :location).order(start_time: :asc).where(start_time: params[:start_date]..params[:end_date]).bim
        end
      end


      desc 'Rate an Event'
      params do
        requires :token, type: String, desc: 'Access Token'
        requires :rating, type: Hash, desc: 'Nested hash with Rating parameters' do
          requires :event_id, type: Integer, desc: 'Event ID'
          requires :artistes, type: Integer, desc: 'Artiste Rating'
          requires :djs, type: Integer, desc: 'DJs Rating'
          requires :venue, type: Integer, desc: 'Venue Rating'
          requires :drinks, type: Integer, desc: 'Drinks Rating'
          optional :food, type: Integer, desc: 'Food Rating'
        end
        optional :event_rating_id, type: Integer, desc: 'Rating ID if modifying existing Rating'
      end
      route ['GET', 'POST'], ':token/rate' do
        authenticate!
        if params.has_key?(:event_rating_id)
          rating = EventRating.find(params[:event_rating_id])
          if rating.update_attributes(declared(params, include_missing: false).rating)
            {message: 'success'}
          else
            {message: 'failed'}
          end
        elsif EventRating.exists?(event_id: params[:rating][:event_id], user_id: @current_user.id)
          rating = EventRating.find_by(event_id: params[:rating][:event_id], user_id: @current_user.id)
          if rating.update_attributes(declared(params).rating)
            {message: 'success'}
          else
            {message: 'failed'}
          end
        else
          @current_user.event_ratings.create(declared(params).rating)
        end
      end

      desc 'Return a specified event'
      params do
        requires :id, type: Integer, desc: 'Event ID'
      end
      get ':id' do
        Event.find(params[:id])
      end
    end

    # Event Type Endpoints
    resource :event_types, desc: 'Event Type Endpoints' do
      desc 'Return the list of Event Types'
      get do
          if (event_types = $redis.get('event_types')).nil?
              event_types = EventType.all
              $redis.set('event_types', event_types.to_json)
          else
              event_types = JSON.parse(event_types)
          end
          event_types
      end
    end

    # Performer Endpoints
    resource :performers, desc: 'Performer Endpoints' do
      desc 'Return the list of performers'
      get do
          if (performers = $redis.get('performers')).nil?
              performers = Performer.all
              $redis.set('performers', performers.to_json)
          else
              performers = JSON.parse(performers)
          end
          performers
      end
    end

    # User Endpoints
    resource :users, desc: 'User Endpoints' do
      desc "Return user's data"
      params do
        requires :token, type: String, desc: 'Access Token'
      end
      get ':token' do
        authenticate!
        @current_user
      end

      desc "Update User's data"
      params do
        requires :token, type: String, desc: 'Access Token'
        requires :user, type: Hash, desc: 'Nested hash with User parameters' do
          optional :first_name, type: String, desc: "User's First Name"
          optional :last_name, type: String, desc: "User's Last Name"
          optional :password, type: String, desc: "New User Password"
          optional :sign_in_count, type: Integer, desc: "Sign In Count"
          optional :country, type: String, desc: "Country Code"
        end
      end
      route ['GET', 'POST'], ':token/update' do
        authenticate!
        if @current_user.update_attributes(declared(params, include_missing: false).user)
          {message: 'success'}
        else
          {message: 'failed'}
        end
      end

      desc "Return user's fliks"
      params do
        requires :token, type: String, desc: 'Access Token'
      end
      get ':token/fliks' do
        authenticate!
        @current_user.user_fliks
      end

      desc "Return user's ratings"
      params do
        requires :token, type: String, desc: 'Access Token'
      end
      get ':token/ratings' do
        authenticate!
        @current_user.event_ratings
      end

      desc "Return user's event list entries"
      params do
        requires :token, type: String, desc: 'Access Token'
      end
      get ':token/list_entries' do
        authenticate!
        @current_user.event_list_entries
      end

      desc 'Join Event List'
      params do
        requires :token, type: String, desc: 'Access Token'
        requires :event_id, type: Integer, desc: 'Event ID'
        requires :first_name, type: String, desc: 'First Name'
        requires :last_name, type: String, desc: 'Last Name'
      end
      post ':token/join_event' do
        authenticate!
        if EventListEntry.exists?(event_id: params[:event_id], user_id: @current_user.id)
          {error: 'List entry for event already exists'}
        else
          @current_user.event_list_entries.create(first_name: params[:first_name], last_name: params['last_name'], event_id: params[:event_id])
        end
      end

      desc 'Add to Fliks'
      params do
        requires :token, type: String, desc: 'Access Token'
        requires :event_id, type: Integer, desc: 'Event ID'
      end
      route ['GET', 'POST'], ':token/add-event-to-fliks' do
        authenticate!
        if UserFlik.exists?(event_id: params[:event_id], user_id: @current_user.id)
          {error: 'Event already part of user fliks'}
        else
          @current_user.user_fliks.create(event_id: params[:event_id])
        end
      end

      desc 'Remove from Fliks'
      params do
        requires :token, type: String, desc: 'Access Token'
        optional :event_id, type: Integer, desc: 'Event ID'
        optional :flik_id, type: Integer, desc: 'Flik ID'
        at_least_one_of :event_id, :flik_id
      end
      route ['GET', 'POST'], ':token/remove-event-from-fliks' do
        authenticate!
        if params.has_key?(:flik_id)
          if UserFlik.find(params[:flik_id]).destroy
            {message: 'success'}
          else
            {message: 'failed'}
          end
        else
          if @current_user.user_fliks.find_by(event_id: params[:event_id]).destroy
            {message: 'success'}
          else
            {message: 'failed'}
          end
        end
      end
    end

    # Location Endpoints
    resource :locations, desc: 'Location Endpoints' do
      desc 'Return list of locations'
      get do
          if (locations = $redis.get('locations')).nil?
              locations = Location.all
              $redis.set('locations', locations.to_json)
          else
              locations = JSON.parse(locations)
          end
          locations
      end

      desc 'Return list of events at specified locations'
      params do
        requires :id, type: Integer, desc: 'Location ID'
      end
      get ':id/events' do
        Event.where({location_id: params[:id]})
      end
    end

    # Authentication Endpoints
    resource :auth, desc: 'Authentication Endpoints' do
      desc 'Register new user. Returns access token if successful'
      params do
        requires :email, type: String, desc: 'E-Mail Address'
        requires :password, type: String, desc: 'Password'
        optional :country, type: String, desc: 'Country Code'
      end
      route ['GET', 'POST'], '/register' do
        if User.exists?(email: params[:email].downcase)
            Keen.publish(:registration_failures, {:email => params[:email].downcase, :password_length => params[:password].length, error: 'E-Mail already exists in database'})
          {error: 'An account with that e-mail address already exists'}
        else
          user = User.create(declared(params))
          Keen.publish(:registration_successes, {:id => user.id, :email => params[:email].downcase, :country => params[:country]})
          {token: user.api_keys.create.access_token}
        end
      end

      desc 'Change user password'
      params do
        requires :token, type: String, desc: 'Access Token'
        requires :new_password, type: String, desc: 'Access Token'
      end
      route ['GET', 'POST'], '/change-password' do
        authenticate!
        @current_user.password = params[:new_password]
        if @current_user.save
          {message: 'success'}
        else
          {message: 'failed'}
        end
      end

      desc 'Checks login credentials. Returns access token specific to user if login credentials are valid | Error: Unauthorized if credentials are not valid'
      params do
        requires :email, type: String, desc: 'User E-mail address'
        requires :password, type: String, desc: 'Password'
      end
      route ['GET', 'POST'], '/login' do
        user = User.find_by_email(params[:email].downcase)
        if !user.nil? && user.valid_password?(params[:password])
          {token: user.api_keys.create.access_token}
        else
            Keen.publish(:login_failures, {:email => params[:email].downcase, :password => params[:password]})
          {error: 'Unauthorized'}
        end
      end

      desc 'Send Password Reset Request'
      params do
        requires :email, type: String, desc: 'User E-Mail Address'
      end
      get 'password/reset' do
        token = ''
        user = User.find_by_email(params[:email].downcase)
        unless user.nil?
          if user.reset_password_token.nil?
            token = user.send_reset_password_instructions
          end
        end
        {token: token}
      end

      desc 'Set New Password After Reset Request'
      params do
        requires :reset_password_token, type: String, desc: 'Reset Token'
        requires :password, type: String, desc: 'New Password'
        requires :password_confirmation, type: String, desc: 'New Password Confirmation'
      end
      route %w(GET POST), 'password/new/:reset_password_token' do
        user = User.reset_password_by_token(declared(params))
        if user.errors.empty?
          {message: 'success'}
        else
          {message: 'Your reset token is either invalid or expired.'}
        end
      end

      desc 'Removes User Access Token (Safe logout procedure)'
      params do
        requires :token, type: String, desc: 'Access Token'
      end
      route ['GET', 'POST', 'DELETE'], ':token/logout' do
        authenticate!
        if ApiKey.find_by_access_token(params[:token]).destroy
          {message: 'success'}
        else
          {message: 'failed'}
        end
      end

      desc 'Returns true if access token is valid | Error: Unauthorized if it is not valid'
      params do
        requires :token, type: String, desc: 'Access Token'
      end
      get ':token/ping' do
        authenticate!
        {message: 'true'}
      end
    end

    # Miscellaneous Endpoints
    desc 'Send Contact Mesage'
    params do
      requires :token, type: String, desc: 'Access Token'
      requires :contact_message, type: Hash, desc: 'Nested has with Message parameters' do
        requires :subject, type: String, desc: 'Message Subject'
        requires :message, type: String, desc: 'Message Content'
      end
    end
    route ['GET', 'POST'], '/contact' do
      authenticate!
      if @current_user.contact_messages.create(declared(params).contact_message)
        {message: 'success'}
      else
        {message: 'failed'}
      end
    end

    add_swagger_documentation hide_format: true,
                              api_version: 'v1',
                              hide_documentation_path: true

  end
end
