class ChangeRatingTypeInEvents < ActiveRecord::Migration
  def change
    change_column :events, :rating, :decimal, precision: 10, scale: 2
  end
end
