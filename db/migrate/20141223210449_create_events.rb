class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.string :name
      t.datetime :start_time
      t.datetime :end_time
      t.string :location
      t.integer :ticket_price
      t.string :facebook_page
      t.string :description
      t.string :twitter_page
      t.string :ig_page

      t.timestamps
    end
  end
end
