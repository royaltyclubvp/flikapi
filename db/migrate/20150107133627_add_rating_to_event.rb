class AddRatingToEvent < ActiveRecord::Migration
  def change
    add_column :events, :rating, :decimal
  end
end
