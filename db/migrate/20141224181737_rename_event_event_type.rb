class RenameEventEventType < ActiveRecord::Migration
  def change
    rename_column :events, :event_type, :event_type_id
  end
end
