class CreateContactMessages < ActiveRecord::Migration
  def change
    create_table :contact_messages do |t|
      t.integer :user_id
      t.string :subject
      t.text :message
      t.boolean :read, default: false

      t.timestamps null: false
    end
  end
end
