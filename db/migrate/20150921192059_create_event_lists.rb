class CreateEventLists < ActiveRecord::Migration
  def change
    create_table :event_lists do |t|
      t.integer :user_id
      t.string :first_name
      t.string :last_name
      t.boolean :checked, default: false
      t.integer :event_id

      t.timestamps null: false
    end
  end
end
