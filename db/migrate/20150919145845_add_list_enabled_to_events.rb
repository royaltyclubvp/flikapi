class AddListEnabledToEvents < ActiveRecord::Migration
  def change
    add_column :events, :list_enabled, :boolean, default: false
  end
end
