class ChangeRatingsFormat < ActiveRecord::Migration
  def change
    change_column :event_ratings, :food, :decimal, precision: 10, scale: 2
    change_column :event_ratings, :venue, :decimal, precision: 10, scale: 2
    change_column :event_ratings, :djs, :decimal, precision: 10, scale: 2
    change_column :event_ratings, :artistes, :decimal, precision: 10, scale: 2
    change_column :event_ratings, :drinks, :decimal, precision: 10, scale: 2
  end
end
