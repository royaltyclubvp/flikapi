class AlterColumnEventsLocation < ActiveRecord::Migration
  def change
    def self.up
      change_table :events do |t|
        t.change :location, :integer
      end
    end
    def self.down
      change_table :events do |t|
        t.change :location, :string
      end
    end
  end
end
