class AddEventCategoryToEvents < ActiveRecord::Migration
  def change
    add_column :events, :event_category, :integer
    add_index :events, :event_category
    Event.update_all({event_category: 5})
  end
end
