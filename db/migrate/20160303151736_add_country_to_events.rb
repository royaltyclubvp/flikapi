class AddCountryToEvents < ActiveRecord::Migration
  def up
    add_column :events, :country, :integer
    add_index :events, :country
    Event.find_each do |event|
      event.country = 0
      event.save
    end
  end
end
