class AddCountryToUser < ActiveRecord::Migration
  def up
    add_column :users, :country, :integer
    add_index :users, :country
    User.find_each do |user|
      user.country = 0
      user.save
    end
  end
end
