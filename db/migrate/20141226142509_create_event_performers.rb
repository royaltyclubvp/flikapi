class CreateEventPerformers < ActiveRecord::Migration
  def change
    create_table :event_performers do |t|
      t.integer :event_id
      t.integer :performer_id

      t.timestamps
    end
  end
end
