class ChangeEventLocationName < ActiveRecord::Migration
  def change
    rename_column :events, :location, :location_idr
  end
end
