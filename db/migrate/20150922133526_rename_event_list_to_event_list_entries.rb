class RenameEventListToEventListEntries < ActiveRecord::Migration
  def self.up
    rename_table :event_list_entry, :event_list_entries
  end

  def self.down
    rename_table :event_list_entries, :event_list_entry
  end
end
