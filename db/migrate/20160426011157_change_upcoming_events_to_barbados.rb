class ChangeUpcomingEventsToBarbados < ActiveRecord::Migration
  def change
    Event.upcoming.find_each do |event|
      if event.country == 'ja'
        event.country = 2
        event.save
      end
    end
  end
end
