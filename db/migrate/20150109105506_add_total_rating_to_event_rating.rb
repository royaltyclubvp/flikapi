class AddTotalRatingToEventRating < ActiveRecord::Migration
  def change
    add_column :event_ratings, :total, :decimal, precision: 10, scale: 2
  end
end
