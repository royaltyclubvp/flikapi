class ChangeEventLocationNameFix < ActiveRecord::Migration
  def change
    rename_column :events, :location_idr, :location_id
  end
end
