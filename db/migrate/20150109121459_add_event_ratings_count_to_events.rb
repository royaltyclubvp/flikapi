class AddEventRatingsCountToEvents < ActiveRecord::Migration
  def up
    add_column :events, :event_ratings_count, :integer, default: 0, null: false
  end

  def down
    remove_column :events, :event_ratings_count
  end
end
