class RenameEventList < ActiveRecord::Migration
  def self.up
    rename_table :event_lists, :event_list_entry
  end

  def self.down
    rename_table :event_list_entry, :event_lists
  end
end
