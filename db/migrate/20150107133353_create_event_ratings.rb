class CreateEventRatings < ActiveRecord::Migration
  def change
    create_table :event_ratings do |t|
      t.integer :event_id
      t.integer :user_id
      t.integer :artistes
      t.integer :djs
      t.integer :food
      t.integer :drinks
      t.integer :venue

      t.timestamps
    end
  end
end
