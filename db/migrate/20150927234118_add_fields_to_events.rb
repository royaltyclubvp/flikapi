class AddFieldsToEvents < ActiveRecord::Migration
  def change
    add_column :events, :declined_reason, :text
    add_column :events, :submission_date, :datetime
  end
end
