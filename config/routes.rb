# == Route Map
#
#   Prefix Verb URI Pattern Controller#Action
# flik_api      /           Flik::API
#

Rails.application.routes.draw do
  mount Flik::API => '/'

  devise_for :user
end
