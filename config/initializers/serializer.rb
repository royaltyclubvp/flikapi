# Disable root key for JSON
ActiveModel::Serializer.root = false

# Disable root key for JSON for Array Serializer
ActiveModel::ArraySerializer.root = false